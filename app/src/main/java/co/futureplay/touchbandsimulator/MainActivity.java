package co.futureplay.touchbandsimulator;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;


public class MainActivity extends ActionBarActivity {
    private static final UUID SERIAL_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final String TAG = MainActivity.class.getSimpleName();

    private Thread ioThread;
    private BluetoothServerSocket serverSocket;
    private OutputStream outputStream;
    private InputStream inputStream;
    private MultiTouchData touch;
    private boolean viewSizeDetermined;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        View touchView = findViewById(R.id.touch_area);

        touch = new MultiTouchData(6, 34);

        touchView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (!viewSizeDetermined) {
                    touch.setViewSize(v.getWidth(), v.getHeight());
                    viewSizeDetermined = true;
                }
                touch.onTouchEvent(event);
                updateTouchData();
                return true;
            }
        });

        updateTouchData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        start();
    }

    @Override
    protected void onPause() {
        stop();
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void start() {
        ioThread = new Thread(new Runnable() {
            @Override
            public void run() {
                listen();
            }
        });
        ioThread.start();
    }

    private void stop() {
        if (ioThread != null) {
            ioThread.interrupt();
            ioThread = null;
        }
    }

    private void updateState(final String state) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((TextView) findViewById(R.id.status_text)).setText(state);
            }
        });
    }

    private void updateTouchData() {
        ((TextView) findViewById(R.id.data_text)).setText(touch.toString());
    }

    private void listen() {
        try {
            serverSocket = BluetoothAdapter.getDefaultAdapter().listenUsingRfcommWithServiceRecord("FPBTTerm", SERIAL_UUID);
        } catch (IOException e) {
            Toast.makeText(MainActivity.this, "Failed to listen", Toast.LENGTH_SHORT).show();
            finish();
        }

        while (!Thread.interrupted()) {
            updateState("LISTENING ");
            BluetoothSocket clientSocket = null;
            try {
                clientSocket = serverSocket.accept();
            } catch (IOException e) {
                break;
            }

            try {
                inputStream = clientSocket.getInputStream();
                outputStream = clientSocket.getOutputStream();
            } catch (IOException e) {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e2) {
                    }
                }
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException e2) {
                    }
                }
                continue;
            }

            ioLoop();
        }
    }

    private void ioLoop() {
        updateState("CONNECTED");
        Integer cmd = null;
        while (!Thread.interrupted()) {
            try {
                if (cmd == null) {
                    cmd = inputStream.read();
                } else {
                    if (cmd == 'T') {
                        send();
                    } else {
                        Log.w(TAG, "Unknown command " + cmd);
                    }
                    cmd = null;
                }
            } catch (IOException e) {
                Log.e(TAG, "IO Error", e);
                break;
            }
        }
    }

    private void send() throws IOException {
        outputStream.write(touch.getSendData());
    }
}
